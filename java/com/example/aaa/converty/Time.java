package com.example.dude.converty;

public class Time
{
    private static final double[] timeArray =
    {//unit to minute
        0.0166667, // seconds, 0
        1,// minutes, 1
        60,// hours, 2
        1440, // days, 3
        10080,// weeks, 4
        43800, // months, 5
        525600 // years, 6
    };

    private static final double CONVERT_BASE_INDEX = 1;


    public static double getUnit(int index)
    {
        if(index < timeArray.length)
        {
            return timeArray[index];
        }
        return CONVERT_BASE_INDEX;
    }

    public static double[] getArray()
    {
        return timeArray;
    }

    public static double getBaseIndex()
    {
        return CONVERT_BASE_INDEX;
    }
}
