package com.example.dude.converty;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ConvertTools convertTools;
    private int oldUnit1Selection = 0;
    private int currUnit1Selection = 0;
    private int oldUnit2Selection = 0;
    private int currUnit2Selection = 0;

    private Spinner convertUnitSpinner;
    private Spinner unit1Spinner;
    private Spinner unit2Spinner;

    private ArrayAdapter<CharSequence> convertUnitAdapter;

    private ArrayAdapter<CharSequence> lengthAdapter;
    private ArrayAdapter<CharSequence> timeAdapter;
    private ArrayAdapter<CharSequence> temperatureAdapter;
    private ArrayAdapter<CharSequence> computeAdapter;
    private ArrayAdapter<CharSequence> weightAdapter;
    private ArrayAdapter<CharSequence> volumeAdapter;

    private EditText inputText;
    private EditText outputText;

    private boolean rotating = false; //used for saving edittext values on screen rotation
    private boolean autoConvert = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        convertTools = new ConvertTools();
        setUi();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putString("InputText", inputText.getText().toString());
        savedInstanceState.putString("OutputText", outputText.getText().toString());

        savedInstanceState.putInt("UnitSelection", convertUnitSpinner.getSelectedItemPosition());
        savedInstanceState.putInt("Unit1", unit1Spinner.getSelectedItemPosition());
        savedInstanceState.putInt("Unit2",unit2Spinner.getSelectedItemPosition());

        savedInstanceState.putBoolean("AutoConvert",autoConvert);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        rotating = true;
        String input = savedInstanceState.getString("InputText");
        String output = savedInstanceState.getString("OutputText");

        int unitselect = savedInstanceState.getInt("UnitSelection");
        int unit1 = savedInstanceState.getInt("Unit1");
        int unit2 = savedInstanceState.getInt("Unit2");

        autoConvert = savedInstanceState.getBoolean("AutoConvert");
        convertUnitSpinner.setSelection(unitselect);
        unit1Spinner.setSelection(unit1);
        unit2Spinner.setSelection(unit2);
        inputText.setText(input);
        outputText.setText(output);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        //auto convert on by default
        menu.getItem(0).setChecked(autoConvert);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.autoConvert:
                toggleAutoConvert(item);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setUi() {
        ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.action_bar, null);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        actionBar.setCustomView(actionBarLayout, layoutParams);

        //spinners
        convertUnitSpinner = (Spinner) actionBarLayout.findViewById(R.id.convertUnits);
        convertUnitAdapter = ArrayAdapter.createFromResource(this, R.array.convertUnitArray, R.layout.actionbarspinner);
        convertUnitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        convertUnitSpinner.setAdapter(convertUnitAdapter);

        unit1Spinner = (Spinner) findViewById(R.id.unit1);
        unit2Spinner = (Spinner) findViewById(R.id.unit2);

        lengthAdapter = ArrayAdapter.createFromResource(this, R.array.lengthUnits, R.layout.spinners);
        timeAdapter = ArrayAdapter.createFromResource(this, R.array.timeUnits, R.layout.spinners);
        temperatureAdapter = ArrayAdapter.createFromResource(this, R.array.temperatureUnits, R.layout.spinners);
        computeAdapter = ArrayAdapter.createFromResource(this,R.array.computeUnits, R.layout.spinners);
        weightAdapter = ArrayAdapter.createFromResource(this, R.array.weightUnits, R.layout.spinners);
        volumeAdapter = ArrayAdapter.createFromResource(this, R.array.volumeUnits, R.layout.spinners);

        lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        temperatureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        computeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        volumeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spinners

        //input output
        inputText = (EditText) findViewById(R.id.inputValue);
        outputText = (EditText) findViewById(R.id.convertTextId);

        inputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(autoConvert)
                {
                    doConvert(true);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        //input output

        //unit selection spinner listener
        convertUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!rotating)
                {
                    clearText();
                }

                switch (position) {
                    case 0:
                        unit1Spinner.setAdapter(lengthAdapter);
                        unit2Spinner.setAdapter(lengthAdapter);
                        break;
                    case 1:
                        unit1Spinner.setAdapter(timeAdapter);
                        unit2Spinner.setAdapter(timeAdapter);
                        break;
                    case 2:
                        unit1Spinner.setAdapter(weightAdapter);
                        unit2Spinner.setAdapter(weightAdapter);
                        break;
                    case 3:
                        unit1Spinner.setAdapter(temperatureAdapter);
                        unit2Spinner.setAdapter(temperatureAdapter);
                        break;
                    case 4:
                        unit1Spinner.setAdapter(volumeAdapter);
                        unit2Spinner.setAdapter(volumeAdapter);
                        break;
                    case 5:
                        unit1Spinner.setAdapter(computeAdapter);
                        unit2Spinner.setAdapter(computeAdapter);
                        break;
                }
                unit2Spinner.setSelection(1);
                oldUnit1Selection = unit1Spinner.getSelectedItemPosition();
                oldUnit2Selection = unit2Spinner.getSelectedItemPosition();
                currUnit1Selection = unit1Spinner.getSelectedItemPosition();
                currUnit2Selection = unit2Spinner.getSelectedItemPosition();
                changeConvertUnit(position);
                rotating = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        unit1Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if((convertUnitSpinner.getSelectedItemPosition() == ConvertTools.COMPUTING_INDEX && position == Computing.DECIMAL_INDEX)
                        || convertUnitSpinner.getSelectedItemPosition() <= ConvertTools.HIGHEST_NUMERIC_UNIT_INDEX)
                {
                    //numeric input
                    inputText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                }
                else
                {   //alpha numeric input
                    inputText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                }

                oldUnit1Selection = currUnit1Selection;
                currUnit1Selection = position;
                if(currUnit2Selection == position)
                {
                    unit2Spinner.setSelection(oldUnit1Selection);
                }

                if(autoConvert)
                {
                    doConvert(true);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        unit2Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                oldUnit2Selection = currUnit2Selection;
                currUnit2Selection = position;
                if(currUnit1Selection == position)
                {
                    unit1Spinner.setSelection(oldUnit2Selection);
                }

                if(autoConvert)
                {
                    doConvert(true);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });


        Button clearButton = (Button) findViewById(R.id.clearInputButton);
        Button swapButton = (Button) findViewById(R.id.unitSwapButton);
        Button convertButton = (Button) findViewById(R.id.convertInputButton);

        clearButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearText();
            }
        });

        swapButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                int temp = unit1Spinner.getSelectedItemPosition();
                unit1Spinner.setSelection(unit2Spinner.getSelectedItemPosition());
                unit2Spinner.setSelection(temp);
            }
        });

        convertButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                doConvert(false);
            }
        });
    }

    public void doConvert(boolean isAutoConvert)
    {
        if(canBeConverted())
        {
            String output;
            output =
                    convertTools.convert(inputText.getText() + "",
                            unit1Spinner.getSelectedItemPosition(), unit2Spinner.getSelectedItemPosition()) + "";
            outputText.setText(output);
        }
        else
        {
            outputText.setText("");
            if(!isAutoConvert)
            {
                Toast.makeText(getApplicationContext(), "Invalid input.", Toast.LENGTH_SHORT).show();
            }
            else
            {
                if(!emptyInputText())
                {
                    Toast.makeText(getApplicationContext(), "Current input out of allowed bounds.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void clearText()
    {
        inputText.setText("");
        outputText.setText("");
    }

    public void changeConvertUnit(int newUnit) {
        convertTools.changeCurrentUnit(newUnit);
    }

    public boolean canBeConverted()
    {
        return (convertUnitSpinner.getSelectedItemPosition() <= ConvertTools.HIGHEST_NUMERIC_UNIT_INDEX &&
                isValidNumericInput(inputText.getText() + "")) ||
                (convertUnitSpinner.getSelectedItemPosition() == ConvertTools.COMPUTING_INDEX &&
                        isValidComputingInput(inputText.getText() + ""));
    }

    public boolean emptyInputText()
    {
        return (inputText.getText() + "").isEmpty();
    }

    public boolean isValidInput(String input)
    {
        if(input == null || input.isEmpty())
        {
            return false;
        }
        return true;
    }

    public boolean isValidNumericInput(String input)
    {
        if(!isValidInput(input))
        {
            return false;
        }

        String testOutput;
        try
        {
            Double.parseDouble(input);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        try
        {
            if(Double.isInfinite(Double.parseDouble(convertTools.convert(input,
                    unit1Spinner.getSelectedItemPosition(), unit2Spinner.getSelectedItemPosition()))))
            {
                return false;
            }
        }
        catch(NumberFormatException ex)
        {
            return false;
        }

        return true;
    }

    public boolean isValidComputingInput(String input)
    {
        if(!isValidInput(input))
        {
            return false;
        }

        int curUnit = unit1Spinner.getSelectedItemPosition();
        if((curUnit == 0 && Computing.isBinary(input)) || (curUnit == 1 && Computing.isDecimal(input)) ||
                (curUnit == 2 && Computing.isHexadecimal(input)))
        {
            try
            {
                input = Computing.convertToBinary(input, unit1Spinner.getSelectedItemPosition());
            }
            catch(NumberFormatException ex)
            {
                return false;
            }
            try
            {
                Computing.convertToTarget(input, unit2Spinner.getSelectedItemPosition());
            }
            catch(NumberFormatException ex)
            {
                return false;
            }
            return true;
        }

        return false;
    }

    public void toggleAutoConvert(MenuItem item)
    {
        autoConvert = !autoConvert;
        item.setChecked(autoConvert);
        if(autoConvert)
        {
            doConvert(true);
        }
    }
}