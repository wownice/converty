package com.example.dude.converty;

public class Temperature
{
    public static double fahrenheitToCelsius(double input)
    {
        return (input - 32)/1.8;
    }

    public static double celsiusToFahrenheit(double input)
    {
        return (input * 1.8) + 32;
    }
}
