package com.example.dude.converty;

public class Length
{
    private static final double[] lengthArray =
    {
            0.0393701, //mm, 0
            0.393701, //cm, 1
            39.3701, //m , 2
            39370.1, // km, 3
            1, //in, 4
            12, //ft, 5
            36, //yd, 6
            63360 //mi, 7
    };

    private static final double CONVERT_BASE_INDEX = 4;

    public static double getUnit(int index)
    {
        if(index < lengthArray.length)
        {
            return lengthArray[index];
        }
        return CONVERT_BASE_INDEX;
    }

    public static double[] getArray()
    {
        return lengthArray;
    }

    public static double getBaseIndex()
    {
        return CONVERT_BASE_INDEX;
    }
}
