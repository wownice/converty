package com.example.dude.converty;

public class ConvertTools
{
    private int CURRENT_MEASUREMENT;

    final static int HIGHEST_NUMERIC_UNIT_INDEX = 4;
    static final int COMPUTING_INDEX = 5;
    /*
        length = 0
        time = 1
        weight = 2
        temperature = 3
        volume = 4
        computing = 5
     */

    public void changeCurrentUnit(int newUnit)
    {
        CURRENT_MEASUREMENT = newUnit;
    }

    public String convert(String input, int originalUnit, int targetUnit)
    {
        if (originalUnit == targetUnit)
        {
            return input;
        }

        if (CURRENT_MEASUREMENT <= HIGHEST_NUMERIC_UNIT_INDEX)
        {
            double doubleInput = Double.parseDouble(input);
            double output = 0;
            double baseVal;

            if (CURRENT_MEASUREMENT == 0) //length
            {
                baseVal = convertToBase(doubleInput, Length.getArray()[originalUnit]);
                output = convertToTarget(baseVal, Length.getArray()[targetUnit]);
            }
            else if (CURRENT_MEASUREMENT == 1) //time
            {
                baseVal = convertToBase(doubleInput, Time.getArray()[originalUnit]);
                output = convertToTarget(baseVal, Time.getArray()[targetUnit]);
            }
            else if (CURRENT_MEASUREMENT == 2)//weight
            {
                baseVal = convertToBase(doubleInput, Weight.getArray()[originalUnit]);
                output = convertToTarget(baseVal, Weight.getArray()[targetUnit]);
            }
            else if (CURRENT_MEASUREMENT == 3) //temperature
            {
                if(originalUnit == 0)
                {
                    output = Temperature.fahrenheitToCelsius(doubleInput);
                }
                else
                {
                    output = Temperature.celsiusToFahrenheit(doubleInput);
                }
            }
            else if (CURRENT_MEASUREMENT == 4) //volume
            {
                baseVal = convertToBase(doubleInput, Volume.getArray()[originalUnit]);
                output = convertToTarget(baseVal, Volume.getArray()[targetUnit]);
            }


            return output + "";
        }
        else if(CURRENT_MEASUREMENT == COMPUTING_INDEX)
        {
            String output = Computing.convertToBinary(input, originalUnit);
            output = Computing.convertToTarget(output, targetUnit);

            return output;
        }

        return 0 + "";
    }


    public double convertToBase(double input, double originalUnitValue)
    {
        return input * originalUnitValue;
    }

    public double convertToTarget(double input, double targetUnitValue)
    {
        return input/targetUnitValue;
    }
}