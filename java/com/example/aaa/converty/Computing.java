package com.example.dude.converty;

import java.math.BigInteger;

public class Computing
{
    static final int BINARY_INDEX = 0;
    static final int DECIMAL_INDEX = 1;
    static final int HEX_INDEX = 2;

    public static boolean isBinary(String input)
    {
        return input.matches("[01]+");
    }

    public static boolean isDecimal(String input)
    {
        try
        {
            Integer.parseInt(input);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }

    public static boolean isHexadecimal(String input)
    {
        return input.matches("-?[0-9a-fA-F]+");
    }
	
    public static String hexToBinary(String input)
    {
        return (new BigInteger(input, 16).toString(2)) + "";
    }

    public static String decToBinary(String input)
    {
        return (Integer.toBinaryString(Integer.parseInt(input))) + "";
    }

    public static String binaryToDec(String input)
    {
        return Integer.parseInt(input,2) + "";
    }

    public static String binaryToHex(String input)
    {
        return Integer.toString(Integer.parseInt(binaryToDec(input)), 16);
    }

    public static String convertToBinary(String input, int originalUnit)
    {
        if(originalUnit == DECIMAL_INDEX)
        {
            return decToBinary(input);
        }

        if(originalUnit == HEX_INDEX)
        {
            return hexToBinary(input);
        }

        return input; //if is already binary
    }

    public static String convertToTarget(String input, int targetUnit)
    {
        if(targetUnit == DECIMAL_INDEX)
        {
            return binaryToDec(input);
        }

        if(targetUnit == HEX_INDEX)
        {
            return binaryToHex(input);
        }

        return input; //if target is binary
    }
}
