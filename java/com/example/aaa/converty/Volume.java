package com.example.dude.converty;

public class Volume
{
    private static final double[] volumeArray =
    {
        0.00492892, //us teaspoon
        0.0147868, //us tablespoon
        0.0295735, //us fl oz
        0.24, //us cup
        0.473176, //us pint
        0.946353, //us quart
        3.78541, //us gallon
        0.001, //ml
        1, //liter , 8
        0.0163871, //cubic inch
        28.3168, //cubic ft
        1000 //cubic meter
    };

    private static final double CONVERT_BASE_INDEX = 8;

    public static double getUnit(int index)
    {
        if(index < volumeArray.length)
        {
            return volumeArray[index];
        }
        return CONVERT_BASE_INDEX;
    }

    public static double[] getArray()
    {
        return volumeArray;
    }

    public static double getBaseIndex()
    {
        return CONVERT_BASE_INDEX;
    }
}
