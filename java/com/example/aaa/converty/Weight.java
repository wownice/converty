package com.example.dude.converty;

public class Weight
{
    private static final double[] weightArray =
    {
        0.00220462, //gram, 0
        2.20462, //kg, 1
        0.0625, //oz, 2
        1, //lb, 3
        2000 //ton, 4
    };

    private static final double CONVERT_BASE_INDEX = 3;

    public static double getUnit(int index)
    {
        if(index < weightArray.length)
        {
            return weightArray[index];
        }
        return CONVERT_BASE_INDEX;
    }

    public static double[] getArray()
    {
        return weightArray;
    }

    public static double getBaseIndex()
    {
        return CONVERT_BASE_INDEX;
    }
}
